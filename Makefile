version :=`git describe --abbrev=0 --tags $(git rev-list --tags --max-count=1)`
dockerfile = $(subst .docker,,$@)

all: test lint-full

test:
	@echo ">> test"
	#@go test -coverprofile cover.out
	#@go tool cover -html=cover.out -o cover.html

lint-full: lint card

lint-badge-full: lint card card-badge

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

card-badge:
	@echo ">>card-badge"
	@curl -X GET 'https://goreportcard.com/checks?repo=gitlab.com/shadowy/go/static-server'

build: static-server copy-keys

static-server:
	@echo ">>build: static-server $(version)"
	@mkdir -p ./dist
	@env GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./dist/$@ ./cmd/$@/main.go
	@chmod 777 ./dist/$@

copy-keys:
	@echo ">>copy-keys"
	@mkdir -p ./dist/site
	@cp -r ./data ./dist/data

docker:
	@echo ">>docker"
	docker build --file ./docker/Dockerfile . -t static-server

docker-release: docker-image

docker-login:
	@echo ">>docker-login"
	@echo "${REGISTRY_PASSWORD}" | docker login -u "${REGISTRY_USER}" --password-stdin

docker-image:
	@echo ">>docker-image"
	@docker build --file ./docker/Dockerfile . -t shadowy/static-server:$(version)
	@docker push shadowy/static-server:$(version)

release: docker-login docker-release

cert:
	@echo ">>make certificate"
	@openssl genrsa -out ./data/server.key 2048
	@openssl req -new -x509 -key ./data/server.key -out ./data/server.pem -days 3000
