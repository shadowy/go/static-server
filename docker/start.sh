#!/bin/sh
USE_PORT=${USE_PORT:-3000}
USE_HTTPS=${USE_HTTPS:-false}
PROXY=${PROXY:-}
INDEX_HTML_TRANSFORM=${INDEX_HTML_TRANSFORM:-}
echo "------------------------------------------------------"
echo "USE_PORT=$USE_PORT"
echo "USE_HTTPS=$USE_HTTPS"
echo "PROXY=$PROXY"
echo "INDEX_HTML_TRANSFORM=$INDEX_HTML_TRANSFORM"
echo "------------------------------------------------------"
/app/static-server -port=${USE_PORT} -https=${USE_HTTPS} -index-html-transform=${INDEX_HTML_TRANSFORM} -proxy=${PROXY}
