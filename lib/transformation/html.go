package transformation

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"strings"
)

type HTMLTransform struct {
	EnvVariable    string `yaml:"envVariable"`
	TextForReplace string `yaml:"textForReplace"`
}

const indexHTML = "index.html"

func (t *HTMLTransform) LoadFromFile(fileName string) error {
	logrus.WithFields(logrus.Fields{"fileName": fileName}).Info("HTMLTransform.LoadFromFile")
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"fileName": fileName}).Error("HTMLTransform.LoadFromFile load")
		return err
	}
	err = yaml.Unmarshal(data, t)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"fileName": fileName}).Error("HTMLTransform.LoadFromFile parse")
		return err
	}
	return nil
}

func HTMLTransformExecute(fileName, rootPath string) error {
	logrus.WithFields(logrus.Fields{"fileName": fileName}).Info("HTMLTransform")
	t := new(HTMLTransform)
	var err error
	err = t.LoadFromFile(fileName)
	if err != nil {
		return err
	}
	list, err := ioutil.ReadDir(rootPath)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"rootPath": rootPath}).Error("HTMLTransform ReadDir")
		return err
	}
	for i := range list {
		file := list[i]
		if file.IsDir() {
			continue
		}
		name := file.Name()
		if strings.HasPrefix(name, indexHTML) {
			if name == indexHTML {
				continue
			}
			err = os.Remove(rootPath + name)
			if err != nil {
				logrus.WithError(err).WithFields(logrus.Fields{"rootPath": rootPath, "fileName": name}).
					Error("HTMLTransform remove file")
			}
		}
	}
	data, err := ioutil.ReadFile(rootPath + indexHTML)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"rootPath": rootPath}).Error("HTMLTransform read index.html")
		return err
	}
	text := strings.Replace(string(data), t.TextForReplace, os.Getenv(t.EnvVariable), -1)
	err = ioutil.WriteFile(rootPath+indexHTML, []byte(text), 0600)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"rootPath": rootPath}).Error("HTMLTransform write index.html")
		return err
	}
	return nil
}
