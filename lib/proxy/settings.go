package proxy

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"strings"
)

type Settings struct {
	EndPoints []EndPoint `yaml:"endPoints"`
}

func LoadFromFile(fileName string) (*Settings, error) {
	logrus.WithFields(logrus.Fields{"fileName": fileName}).Info("proxy.LoadFromFile")
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"fileName": fileName}).Error("proxy.LoadFromFile load")
		return nil, err
	}
	var result Settings
	err = yaml.Unmarshal(data, &result)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"fileName": fileName}).Error("proxy.LoadFromFile parse")
		return nil, err
	}
	for i := range result.EndPoints {
		p := strings.TrimRight(strings.TrimLeft(strings.ToLower(result.EndPoints[i].Path), " "), " ")
		result.EndPoints[i].Path = p
	}
	return &result, nil
}
