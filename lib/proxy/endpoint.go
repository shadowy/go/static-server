package proxy

type EndPoint struct {
	Path string `yaml:"path"`
	URL  string `yaml:"url"`
}
