package main

import (
	"crypto/md5"
	"crypto/tls"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/toorop/gin-logrus"
	"gitlab.com/shadowy/go/static-server/lib/proxy"
	"gitlab.com/shadowy/go/static-server/lib/transformation"
	"mime"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var supportCompression = []string{"br", "gz"}
var supportEncoding = []string{"br", "gzip"}
var eTag = fmt.Sprintf("%x", md5.Sum([]byte(time.Now().String())))

var port = flag.Int("port", 3000, "port for serve")
var https = flag.Bool("https", false, "use https for serve")
var proxyFile = flag.String("proxy", "", "file with proxy settings")
var indexHTMLTransform = flag.String("index-html-transform", "", "settings for index html transformation")
var rootPath = flag.String("root", "./site/", "folder of site root")

func main() {
	var err error
	logrus.Info("static-server starting")
	logrus.SetLevel(logrus.DebugLevel)
	flag.Parse()
	logrus.WithFields(logrus.Fields{
		"port":               *port,
		"https":              *https,
		"proxyFile":          *proxyFile,
		"rootPath":           *rootPath,
		"indexHTMLTransform": *indexHTMLTransform,
	}).Info("static-server params")
	var data *proxy.Settings
	if proxyFile != nil && *proxyFile != "" {
		data, _ = proxy.LoadFromFile(*proxyFile)
	}
	if indexHTMLTransform != nil && *indexHTMLTransform != "" {
		err = transformation.HTMLTransformExecute(*indexHTMLTransform, *rootPath)
		if err != nil {
			panic(err)
		}
	}

	err = start(data)
	if err != nil {
		logrus.WithError(err).Error("static-server gin")
		panic(err)
	}
}

func sendFile(ctx *gin.Context, file string) {
	encoding := strings.ToLower(ctx.GetHeader("Accept-Encoding"))
	for i := range supportCompression {
		ext := supportCompression[i]
		enc := supportEncoding[i]
		if strings.Contains(encoding, enc) {
			if _, err := os.Stat(file + "." + ext); !os.IsNotExist(err) {
				ctx.Header("Content-Type", mime.TypeByExtension(filepath.Ext(file)))
				ctx.Header("Content-Encoding", enc)
				ctx.File(file + "." + ext)
				return
			}
		}
	}

	if _, err := os.Stat(file); os.IsNotExist(err) {
		ctx.File(*rootPath + "index.html")
		return
	}
	ctx.File(file)
}

func executeProxy(ctx *gin.Context, data *proxy.Settings) bool {
	if data == nil || len(data.EndPoints) == 0 {
		return false
	}
	logrus.WithFields(logrus.Fields{"path": ctx.Request.URL.Path}).Debug("executeProxy")
	for i := range data.EndPoints {
		end := data.EndPoints[i]
		if strings.Index(strings.ToLower(ctx.Request.RequestURI), end.Path) == 0 {
			execute(ctx, end)
			return true
		}
	}
	return false
}

func execute(ctx *gin.Context, endpoint proxy.EndPoint) {
	request := ctx.Request
	urlNew, _ := url.Parse(endpoint.URL)
	urlPath := request.URL.Path[len(endpoint.Path):]
	if urlPath != "" {
		urlNew.Path = path.Join(urlNew.Path, urlPath)
	}
	logrus.WithFields(logrus.Fields{"url": urlNew.String(), "add-path": urlPath}).Debug("execute")
	pr := httputil.NewSingleHostReverseProxy(urlNew)
	request.Header.Set("X-Forwarder-Host", request.Header.Get("Host"))
	request.URL.Scheme = urlNew.Scheme
	request.URL.Path = urlNew.Path
	request.Host = urlNew.Host
	pr.ServeHTTP(ctx.Writer, request)
}

func start(data *proxy.Settings) error {
	r := gin.Default()
	r.Use(ginlogrus.Logger(logrus.StandardLogger()), gin.Recovery())

	r.NoRoute(func(ctx *gin.Context) {
		if data != nil && executeProxy(ctx, data) {
			return
		}
		dir, file := path.Split(ctx.Request.URL.Path)
		logrus.WithFields(logrus.Fields{"dir": dir, "file": file}).Debug("NoRoute")
		ext := filepath.Ext(file)
		ctx.Header("Cache-Control", "private, max-age=31536000")
		ctx.Header("ETag", eTag)
		if match := ctx.GetHeader("If-None-Match"); match != "" {
			if strings.Contains(match, eTag) {
				ctx.Status(http.StatusNotModified)
				return
			}
		}
		if file == "" || ext == "" {
			sendFile(ctx, *rootPath+"index.html")
		} else {
			sendFile(ctx, *rootPath+path.Join(dir, file))
		}
	})

	var err error
	if *https {
		server := http.Server{
			Addr:    ":" + strconv.Itoa(*port),
			Handler: r,
			TLSConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		}
		err = server.ListenAndServeTLS("./data/server.pem", "./data/server.key")
		//err = r.RunTLS(":"+strconv.Itoa(*port), "./data/server.pem", "./data/server.key")
	} else {
		err = r.Run(":" + strconv.Itoa(*port))
	}
	return err
}
