# CHANGELOG

<!--- next entry here -->

## 1.2.3
2020-08-28

### Fixes

- fix start.sh script (3ebe5fb85fff563b4e2d9e78183d7477379e40a0)

## 1.2.2
2020-08-28

### Fixes

- add logs (add5f7675efff04d65bf627a988e2eedddd4ede5)

## 1.2.1
2020-08-28

### Fixes

- update script on docker file (107d24df690dbf0d749d22b2f988f83ca104bc44)
- update script on docker file (61e576a13fef9cd82cdc926699759069e4f4f2f3)

## 1.2.0
2020-08-28

### Features

- add html transformation (4096f3a9e6d033883cbbf11e5bb981c24981ca62)

## 1.1.1
2020-01-14

### Fixes

- issue with file path (b5ed92eb21be159793595599cf79c503667d21d3)