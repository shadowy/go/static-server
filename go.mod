module gitlab.com/shadowy/go/static-server

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/sirupsen/logrus v1.4.2
	github.com/toorop/gin-logrus v0.0.0-20190701131413-6c374ad36b67
	gopkg.in/yaml.v2 v2.2.2
)
